#pragma once

#include <boost/asio.hpp>
#include <string>
#include <boost/noncopyable.hpp>
#include "connection.h"
#include "connection_manager.h"

namespace NNetwork {
    namespace NHTTP {


        class TServer
                : private boost::noncopyable
        {
        public:
            explicit TServer(const std::string& address, const std::string& port, std::shared_ptr<IRequestHandler> requestHandler);

            void Run();

            void Stop();

        private:
            void HandleAccept(const boost::system::error_code& e);

            void HandleStop();

            boost::asio::io_service IOService;

            boost::asio::ip::tcp::acceptor Acceptor;

            TConnectionManager ConnectionManager;

            std::shared_ptr<IRequestHandler> RequestHandler;

            connection_ptr NewConnection;
        };
    }
}
