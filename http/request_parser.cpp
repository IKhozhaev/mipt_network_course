#include "request_parser.h"
#include "header.h"

namespace NNetwork {
    namespace NHTTP {

        TRequestParser::TRequestParser()
                : State(method_start), ContentLength(0) {}

        void TRequestParser::Reset() {
            State = method_start;
            ContentLength = 0;
        }

        void TRequestParser::CalculateContentLength(const TRequest& req) {
            ContentLength = 0;
            for (const THeader& header : req.Headers) {
                if (header.Name == "Content-Length") {
                    ContentLength = std::stoull(header.Value);
                }
            }
        }

        boost::tribool TRequestParser::ReadNext(TRequest& req, TChar c) {
            switch (State) {
            case method_start:
                if (!IsChar(c) || IsControl(c) || IsSpecial(c)) {
                    return false;
                } else {
                    State = method;
                    req.Method.push_back(c);
                    return boost::indeterminate;
                }
            case method:
                if (c == ' ') {
                    State = uri;
                    return boost::indeterminate;
                } else if (!IsChar(c) || IsControl(c) || IsSpecial(c)) {
                    return false;
                } else {
                    req.Method.push_back(c);
                    return boost::indeterminate;
                }
            case uri_start:
                if (IsControl(c)) {
                    return false;
                } else {
                    State = uri;
                    req.Uri.push_back(c);
                    return boost::indeterminate;
                }
            case uri:
                if (c == ' ') {
                    State = http_version_h;
                    return boost::indeterminate;
                } else if (IsControl(c)) {
                    return false;
                } else {
                    req.Uri.push_back(c);
                    return boost::indeterminate;
                }
            case http_version_h:
                if (c == 'H') {
                    State = http_version_t_1;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_t_1:
                if (c == 'T') {
                    State = http_version_t_2;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_t_2:
                if (c == 'T') {
                    State = http_version_p;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_p:
                if (c == 'P') {
                    State = http_version_slash;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_slash:
                if (c == '/') {
                    req.HTTPVersionMajor = 0;
                    req.HTTPVersionMinor = 0;
                    State = http_version_major_start;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_major_start:
                if (IsDigit(c)) {
                    req.HTTPVersionMajor = req.HTTPVersionMajor * 10 + c - '0';
                    State = http_version_major;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_major:
                if (c == '.') {
                    State = http_version_minor_start;
                    return boost::indeterminate;
                } else if (IsDigit(c)) {
                    req.HTTPVersionMajor = req.HTTPVersionMajor * 10 + c - '0';
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_minor_start:
                if (IsDigit(c)) {
                    req.HTTPVersionMinor = req.HTTPVersionMinor * 10 + c - '0';
                    State = http_version_minor;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case http_version_minor:
                if (c == '\r') {
                    State = expecting_newline_1;
                    return boost::indeterminate;
                } else if (IsDigit(c)) {
                    req.HTTPVersionMinor = req.HTTPVersionMinor * 10 + c - '0';
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case expecting_newline_1:
                if (c == '\n') {
                    State = header_line_start;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case header_line_start:
                if (c == '\r') {
                    State = expecting_newline_3;
                    return boost::indeterminate;
                } else if (!req.Headers.empty() && (c == ' ' || c == '\t')) {
                    State = header_lws;
                    return boost::indeterminate;
                } else if (!IsChar(c) || IsControl(c) || IsSpecial(c)) {
                    return false;
                } else {
                    req.Headers.push_back(THeader());
                    req.Headers.back().Name.push_back(c);
                    State = header_name;
                    return boost::indeterminate;
                }
            case header_lws:
                if (c == '\r') {
                    State = expecting_newline_2;
                    return boost::indeterminate;
                } else if (c == ' ' || c == '\t') {
                    return boost::indeterminate;
                } else if (IsControl(c)) {
                    return false;
                } else {
                    State = header_value;
                    req.Headers.back().Value.push_back(c);
                    return boost::indeterminate;
                }
            case header_name:
                if (c == ':') {
                    State = space_before_header_value;
                    return boost::indeterminate;
                } else if (!IsChar(c) || IsControl(c) || IsSpecial(c)) {
                    return false;
                } else {
                    req.Headers.back().Name.push_back(c);
                    return boost::indeterminate;
                }
            case space_before_header_value:
                if (c == ' ') {
                    State = header_value;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case header_value:
                if (c == '\r') {
                    State = expecting_newline_2;
                    return boost::indeterminate;
                } else if (IsControl(c)) {
                    return false;
                } else {
                    req.Headers.back().Value.push_back(c);
                    return boost::indeterminate;
                }
            case expecting_newline_2:
                if (c == '\n') {
                    State = header_line_start;
                    return boost::indeterminate;
                } else {
                    return false;
                }
            case expecting_newline_3:
                if (c != '\n') {
                    return false;
                }
                CalculateContentLength(req);
                if (ContentLength) {
                    if (req.Method == "GET") {
                        return false;
                    }
                    State = read_request_body;
                    return boost::indeterminate;
                } else {
                    return true;
                }

            case read_request_body:
                if (!IsChar(c)) {
                    return false;
                }
                req.RequestBody.push_back(c);
                --ContentLength;
                if (!ContentLength) {
                    return true;
                } else {
                    return boost::indeterminate;
                }
            default:
                return false;
            }
        }

        bool TRequestParser::IsChar(TChar c) {
            return true;
        }

        bool TRequestParser::IsControl(TChar c) {
            return (c >= 0 && c <= 31) || (c == 127);
        }

        bool TRequestParser::IsDigit(TChar c) {
            return c >= '0' && c <= '9';
        }

        bool TRequestParser::IsSpecial(TChar c) {
            switch (c) {
            case '(':
            case ')':
            case '<':
            case '>':
            case '@':
            case ',':
            case ';':
            case ':':
            case '\\':
            case '"':
            case '/':
            case '[':
            case ']':
            case '?':
            case '=':
            case '{':
            case '}':
            case ' ':
            case '\t':
                return true;
            default:
                return false;
            }
        }

    }
}
