#include "connection.h"
#include <utility>
#include <vector>
#include <boost/bind.hpp>
#include <boost/tuple/tuple.hpp>
#include "connection_manager.h"

namespace NNetwork {
    namespace NHTTP {

        TConnection::TConnection(boost::asio::io_service& io_service,
                               TConnectionManager& manager, std::shared_ptr<IRequestHandler> handler)
                : Socket(io_service),
                  ConnectionManager(manager),
                  RequestHandler(std::move(handler)) {
        }

        boost::asio::ip::tcp::socket& TConnection::GetSocket() {
            return Socket;
        }

        void TConnection::Start() {
            Socket.async_read_some(boost::asio::buffer(Buffer),
                                    boost::bind(&TConnection::HandleRead, shared_from_this(),
                                                boost::asio::placeholders::error,
                                                boost::asio::placeholders::bytes_transferred));
        }

        void TConnection::Stop() {
            Socket.close();
        }

        void TConnection::HandleRead(const boost::system::error_code& e,
                                     std::size_t bytes_transferred) {
            if (!e) {
                boost::tribool result;
                boost::tie(result, boost::tuples::ignore) = RequestParser.Parse(
                        Request, Buffer.data(), Buffer.data() + bytes_transferred);

                if (result) {
                    Reply = RequestHandler->Exec(Request);

                    boost::asio::async_write(Socket, Reply.to_buffers(),
                                             boost::bind(&TConnection::HandleWrite, shared_from_this(),
                                                         boost::asio::placeholders::error));
                } else if (!result) {
                    Reply = RequestHandler->BadRequest(Request);
                    boost::asio::async_write(Socket, Reply.to_buffers(),
                                             boost::bind(&TConnection::HandleWrite, shared_from_this(),
                                                         boost::asio::placeholders::error));
                } else {
                    Socket.async_read_some(boost::asio::buffer(Buffer),
                                            boost::bind(&TConnection::HandleRead, shared_from_this(),
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred));
                }
            } else if (e != boost::asio::error::operation_aborted) {
                ConnectionManager.Stop(shared_from_this());
            }
        }

        void TConnection::HandleWrite(const boost::system::error_code& e) {
            if (!e) {
                boost::system::error_code ignored_ec;
                Socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
            }

            if (e != boost::asio::error::operation_aborted) {
                ConnectionManager.Stop(shared_from_this());
            }
        }

    }
}
