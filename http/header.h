#pragma once

#include<string>

namespace NNetwork {
    namespace NHTTP {
        struct THeader {
            std::string Name;
            std::string Value;
        };
    }
}