#include "server.h"
#include <boost/bind.hpp>
#include <utility>

namespace NNetwork {
    namespace NHTTP {

        TServer::TServer(const std::string& address, const std::string& port,
                         std::shared_ptr<IRequestHandler> requestHandler)
                : IOService(),
                  Acceptor(IOService),
                  ConnectionManager(),

                  NewConnection(new TConnection(IOService,
                                                ConnectionManager, RequestHandler)),
                  RequestHandler(std::move(requestHandler))
        {
            boost::asio::ip::tcp::resolver resolver(IOService);
            boost::asio::ip::tcp::resolver::query query(address, port);
            boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
            Acceptor.open(endpoint.protocol());
            Acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
            Acceptor.bind(endpoint);
            Acceptor.listen();
            Acceptor.async_accept(NewConnection->GetSocket(),
                                  boost::bind(&TServer::HandleAccept, this,
                                              boost::asio::placeholders::error));
        }

        void TServer::Run() {
            IOService.run();
        }

        void TServer::Stop() {
            IOService.post(boost::bind(&TServer::HandleStop, this));
        }

        void TServer::HandleAccept(const boost::system::error_code& e) {
            if (!e) {
                ConnectionManager.Start(NewConnection);
                NewConnection.reset(new TConnection(IOService,
                                                    ConnectionManager, RequestHandler));
                Acceptor.async_accept(NewConnection->GetSocket(),
                                      boost::bind(&TServer::HandleAccept, this,
                                                  boost::asio::placeholders::error));
            }
        }

        void TServer::HandleStop() {
            Acceptor.close();
            ConnectionManager.StopAll();
        }

    }
}
