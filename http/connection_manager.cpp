#include "connection_manager.h"
#include <algorithm>
#include <boost/bind.hpp>

namespace NNetwork {
    namespace NHTTP {

        void TConnectionManager::Start(connection_ptr c) {
            Connections.insert(c);
            c->Start();
        }

        void TConnectionManager::Stop(connection_ptr c) {
            Connections.erase(c);
            c->Stop();
        }

        void TConnectionManager::StopAll() {
            for (auto connectionPtr : Connections) {
                connectionPtr->Stop();
            }
            Connections.clear();
        }

    }
}
