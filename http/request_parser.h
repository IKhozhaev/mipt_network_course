#pragma once

#include "request.h"
#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>

namespace NNetwork {
    namespace NHTTP {

        class TRequestParser {
        public:
            TRequestParser();

            void Reset();

            template<typename InputIt>
            boost::tuple<boost::tribool, InputIt> Parse(TRequest& req, InputIt begin, InputIt end) {
                while (begin != end) {
                    boost::tribool result = ReadNext(req, *begin++);
                    if (result || !result)
                        return boost::make_tuple(result, begin);
                }
                boost::tribool result = boost::indeterminate;
                return boost::make_tuple(result, begin);
            }

        private:
            typedef char TChar;

            uint64_t ContentLength = 0;

            void CalculateContentLength(const TRequest& req);

            boost::tribool ReadNext(TRequest& req, TChar c);

            static bool IsChar(TChar c);

            static bool IsDigit(TChar c);

            static bool IsSpecial(TChar c);

            static bool IsControl(TChar c);

            enum state {
                method_start,
                method,
                uri_start,
                uri,
                http_version_h,
                http_version_t_1,
                http_version_t_2,
                http_version_p,
                http_version_slash,
                http_version_major_start,
                http_version_major,
                http_version_minor_start,
                http_version_minor,
                expecting_newline_1,
                header_line_start,
                header_lws,
                header_name,
                space_before_header_value,
                header_value,
                expecting_newline_2,
                expecting_newline_3,
                read_request_body
            } State;
        };
    }
}