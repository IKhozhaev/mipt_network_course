#pragma once

#include "reply.h"
#include "request.h"

namespace NNetwork {
    namespace NHTTP {
        class IRequestHandler {
        public:
            virtual ~IRequestHandler() = default;
            virtual TReply Exec (const TRequest& request) = 0;
            virtual TReply BadRequest (const TRequest& request) = 0;
        };
    }
}