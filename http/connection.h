#pragma once

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "reply.h"
#include "request.h"
#include "request_parser.h"
#include "iface.h"

namespace NNetwork {
    namespace NHTTP {

        class TConnectionManager;

        class TConnection
                : public boost::enable_shared_from_this<TConnection>,
                  private boost::noncopyable {
        public:
            explicit TConnection(boost::asio::io_service& io_service,
                                TConnectionManager& manager, std::shared_ptr<IRequestHandler> handler);

            boost::asio::ip::tcp::socket& GetSocket();

            void Start();

            void Stop();

        private:
            void HandleRead(const boost::system::error_code& e,
                            std::size_t bytes_transferred);

            void HandleWrite(const boost::system::error_code& e);

            boost::asio::ip::tcp::socket Socket;

            TConnectionManager& ConnectionManager;

            std::shared_ptr<IRequestHandler> RequestHandler;

            boost::array<char, 8192> Buffer;

            TRequest Request;

            TRequestParser RequestParser;

            TReply Reply;
        };

        typedef boost::shared_ptr<TConnection> connection_ptr;

    }
}