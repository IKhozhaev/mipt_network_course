#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "header.h"

namespace NNetwork {
    namespace NHTTP {
        struct TRequest {
            std::string Method;
            std::string Uri;
            int HTTPVersionMajor;
            int HTTPVersionMinor;
            std::vector<THeader> Headers;
            std::string RequestBody;
        };
    }
}