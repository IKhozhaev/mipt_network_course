#pragma once

#include <set>
#include <boost/noncopyable.hpp>
#include "connection.h"

namespace NNetwork {
    namespace NHTTP {

        class TConnectionManager : private boost::noncopyable
        {
        public:
            void Start(connection_ptr c);

            void Stop(connection_ptr c);

            void StopAll();

        private:
            std::set<connection_ptr> Connections;
        };
    }
}
