#include <iostream>
#include <string>
#include <memory>

#include <http/iface.h>
#include <http/server.h>

#include <curl/curl.h>

#include <lib/json_parser.h>

using namespace NNetwork::NHTTP;

class TMyRequestHandler : public IRequestHandler {
private:

    static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
        ((std::string*)userp)->append((char*)contents, size * nmemb);
        return size * nmemb;
    }
public:
    TMyRequestHandler(std::string key)
        : Openweather("https://api.openweathermap.org/data/2.5/weather?")
        , ApiKey(key)
    {}
    TReply Exec (const TRequest& request) final {
        try {
            std::string city_name = request.RequestBody;
            std::string openweather_request = Openweather + "q=" + city_name + "&appid=" + ApiKey;
            CURL *curl = curl_easy_init();
            if(curl) {
                std::string buffer;
                curl_easy_setopt(curl, CURLOPT_URL, openweather_request.c_str());
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
                curl_easy_perform(curl);
                curl_easy_cleanup(curl);

                auto weather_response = Json::Load(buffer);
                auto weather_info = weather_response.GetRoot().AsMap().at("weather").AsArray().at(0).AsMap().at("main").AsString();

                // std::cout << buffer << std::endl;
                weather_info += "\n";

                TReply res;
                res.Status = NNetwork::NHTTP::TReply::ok;
                res.content = weather_info;
                res.headers.push_back({"content-lenght", std::to_string(weather_info.size())});
                return res;
            }

            TReply res;
            res.Status = NNetwork::NHTTP::TReply::internal_server_error;
            return res;
        } catch(...) {
            TReply res;
            res.Status = NNetwork::NHTTP::TReply::internal_server_error;
            return res;
        }

    }
    TReply BadRequest (const TRequest& request) final {
        TReply res;
        res.Status = NNetwork::NHTTP::TReply::bad_request;
        return res;
    }
private:
    const std::string Openweather;
    const std::string ApiKey;
};

int main() {
    std::shared_ptr<TMyRequestHandler> handler(new TMyRequestHandler("668daeaf8be9e67e2dabfe15e5cb3724"));
    TServer server("localhost", "9981", handler);
    server.Run();
    return 0;
}
