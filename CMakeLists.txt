cmake_minimum_required(VERSION 3.22)
project(network)

set(CMAKE_CXX_STANDARD 17)

#--------------------------------------boost---------------------------------------------
set(BOOST_ROOT /Users/ikhozhaev/MIPT/7semestr/network/boost)
set(Boost_USE_STATIC_LIBS ON)
find_package(Boost)

include_directories(${Boost_INCLUDE_DIR})
#----------------------------------------------------------------------------------------

set(CURL_LIBRARY "-lcurl")
find_package(CURL REQUIRED)
include_directories(${CURL_INCLUDE_DIR})


unset(PROJECT_ROOT_PATH)
set(PROJECT_ROOT_PATH "~/MIPT/7semestr/network")

add_subdirectory(http)
add_subdirectory(lib)


add_executable(network main.cpp)

include_directories(${PROJECT_ROOT_PATH})

target_link_libraries(network ${Boost_LIBRARIES} ${CURL_LIBRARIES} http lib)